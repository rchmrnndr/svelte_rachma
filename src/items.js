export default [
  {
    name: "Nike Air Force 1 White-Black",
    price: "2,000,000",
    img: "nike1.jpg",
  },
  {
    name: "Nike Air Forca 1 White-Grey",
    price: "2,000,000",
    img: "nike2.jpg",
  },
  {
    name: "Nike for Woman Pink",
    price: "1,700,000",
    img: "nike3.jpg",
  },
  {
    name: "Nike for Woman Brown",
    price: "1,500,000",
    img: "nike4.jpg",
  },
  {
    name: "Nike Dunks",
    price: "2,000,000",
    img: "nike5.jpg",
  },
  {
    name: "Nike Blazer's",
    price: "1,900,000",
    img: "nike6.jpg",
  },
  {
    name: "Nike Air Force 1 Grey-Black",
    price: "2,670,000",
    img: "nike7.jpg",
  },
  {
    name: "Nike X Dior Air Jordan 1",
    price: "9,000,000",
    img: "nike8.jpg",
  },
  {
    name: "Nike Shoes",
    price: "2,800,00",
    img: "nike9.jpg",
  },
];
